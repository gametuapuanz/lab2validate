module.exports = {
  isUserNameValid: function (username) {
    if (username.length < 3 || username.length > 15) {
      return false;
    }
    if (username.toLowerCase() !== username) {
      return false;
    }
    return true;
  },
  isAgeValid: function (age) {
    if (isNaN(parseInt(age))) {
      return false;
    }
    if (parseInt(age) < 18 || parseInt(age) > 100) {
      return false;
    }
    return true;
  },
  isPasswordValidd: function (password) {
    if (password.length < 8) {
      return false;
    }
    if (!password.match(/[A-Z]{1}/)) {
      return false;
    } if (!password.match(/[0-9]{3}/)) {
      return false;
    } if (!password.match(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]{1}/)) {
      return false;
    }

    return true;
  },

  isDateValid: function (day, month, year) {
      if (day < 1 || day > 31 || month > 12 || year < 1970 || year > 2020) {
        return false;
      }
      if ((month = 1 && day < 32)) {
        return true;
      }
      if ((month = 2 && day < 30)) {
        return true;
      }
      if ((month = 3 && day < 31)) {
        return true;
      }
      if ((month = 4 && day < 30)) {
        return true;
      }
      if ((month = 5 && day < 31)) {
        return true;
      }
      if ((month = 6 && day < 30)) {
        return true;
      }
      if ((month = 7 && day < 31)) {
        return true;
      }
      if ((month = 8 && day < 31)) {
        return true;
      }
      if ((month = 9 && day < 30)) {
        return true;
      }
      if ((month = 10 && day < 31)) {
        return true;
      }
      if ((month = 11 && day < 30)) {
        return true;
      }
      if ((month = 12 && day < 31)) {
        return true;
      }

      return true;
  }
};
